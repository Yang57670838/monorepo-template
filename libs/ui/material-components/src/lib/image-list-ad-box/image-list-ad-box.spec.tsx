import { render } from '@testing-library/react';

import ImageListAdBox from './image-list-ad-box';

describe('ImageListAdBox', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ImageListAdBox />);
    expect(baseElement).toBeTruthy();
  });
});
