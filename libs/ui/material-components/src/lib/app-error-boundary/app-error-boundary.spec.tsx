import '@testing-library/jest-dom';

import { render, screen } from '@testing-library/react';
import AppErrorBoundary from './app-error-boundary';

describe('AppErrorBoundary component', () => {
  it('show replacement', () => {
    const ThrowError = () => {
      throw new Error('Test');
    };
    render(
      <AppErrorBoundary>
        <ThrowError />
      </AppErrorBoundary>
    );
    expect(
      screen.getByText('Something went wrong.. Please contact our')
    ).toBeVisible();
  });

  it('show original component without error', () => {
    const ThrowNoError = () => {
      return <div>no error</div>;
    };
    render(
      <AppErrorBoundary>
        <ThrowNoError />
      </AppErrorBoundary>
    );
    expect(
      screen.queryByText('Something went wrong.. Please contact our')
    ).not.toBeInTheDocument();
    expect(screen.getByText('no error')).toBeVisible();
  });
});
