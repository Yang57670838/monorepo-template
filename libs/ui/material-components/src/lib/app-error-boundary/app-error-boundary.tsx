import React, { Component, ErrorInfo, ReactNode } from 'react';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
interface Props {
  children?: ReactNode;
}
interface State {
  hasError: boolean;
}
class AppErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }
  public static getDerivedStateFromError(_: Error): State {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }
  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // TODO: send error details to backend log service
    console.error('Uncaught error:', error, errorInfo);
  }
  public render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return (
        <Typography
          variant="subtitle2"
          component="h2"
          sx={{ fontWeight: 'bold', fontSize: '16px' }}
        >
          Something went wrong.. Please contact our
          <Link
            href="https://myns.com.au/contact"
            variant="body2"
            sx={{ paddingLeft: '7px' }}
          >
            customer service.
          </Link>
        </Typography>
      );
    }
    return children;
  }
}

export default AppErrorBoundary;
