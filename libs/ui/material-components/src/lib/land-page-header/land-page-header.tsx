import { FC, useRef, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/system';
import styles from './land-page-header.module.css';

/* eslint-disable-next-line */
export interface LandPageHeaderProps {
  projectName: string;
}

// TODO: move to middile of page, change emoji, design better style..
export const LandPageHeader: FC<LandPageHeaderProps> = ({ projectName }) => {
  const h1Ref = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    if (h1Ref.current) {
      h1Ref.current.focus();
    }
  }, []);
  return (
    <div>
      <Typography variant="h3" component="h1" ref={h1Ref} tabIndex={-1}>
        <Box
          sx={{
            display: 'block',
            fontSize: '1.875rem',
            fontWeight: '300',
            marginBottom: '0.5rem',
          }}
        >
          Hello there,
        </Box>
        Happy {projectName}👋
      </Typography>
    </div>
  );
};

export default LandPageHeader;
