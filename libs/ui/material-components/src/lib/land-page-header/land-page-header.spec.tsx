import { render } from '@testing-library/react';

import LandPageHeader from './land-page-header';

describe('LandPageHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LandPageHeader projectName="projectName" />);
    expect(baseElement).toBeTruthy();
  });
});
