import { render } from '@testing-library/react';

import InlineVerifyInputField from './inline-verify-input-field';

describe('InlineVerifyInputField', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<InlineVerifyInputField />);
    expect(baseElement).toBeTruthy();
  });
});
