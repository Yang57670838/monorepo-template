import { FC } from 'react';
import { Box } from '@mui/system';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import styles from './inline-verify-input-field.module.css';

/* eslint-disable-next-line */
export interface InlineVerifyInputFieldProps {
  label: string;
  id: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  verifyEventHandler: () => void;
}

export const InlineVerifyInputField: FC<InlineVerifyInputFieldProps> = ({
  label,
  id,
  onChange,
  value,
  verifyEventHandler,
}) => {
  return (
    <div className={styles['container']}>
      <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
        <TextField
          id={`input-with-verify-${id}`}
          label={label}
          variant="standard"
          onChange={onChange}
          value={value}
        />
        <Button variant="text" onClick={verifyEventHandler}>
          Verify
        </Button>
      </Box>
    </div>
  );
};

export default InlineVerifyInputField;
