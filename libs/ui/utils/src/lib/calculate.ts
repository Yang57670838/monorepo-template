export const getSumOfNumArray = (arr: Array<number>): number => {
  if (arr.length === 0) {
    return 0;
  }

  const firstElement = arr[0];
  const restOfArray = arr.slice(1);
  return firstElement + getSumOfNumArray(restOfArray);
};
