const regexEmail = /^[\w.-]+@([\da-zA-Z.-]+)\.([a-zA-Z.]{2,6})$/;
const regexPhoneAU = /^(?:\+?61)?(?:\(0\)[23478]|\(?0?[23478]\)?)\d{8}$/;
const regexPhone = /^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
const regexMobile = /(^04\d{2}\d{6}$)/;
const regexSpecialChar = /[!@#$%^&*(),.?":{}|<>]/;
const regexAlphaNumeric = /^\w+$/;
const regexNumeric = /^\d*$/;
const regexPhoneInputs = /^[\d|+|-]*$/;
const regexPercentageRange = /\b^([0-9]|[1-8][0-9]|9[0-9]|100)\b/;
const regexPostcode = /^(?:(?:[2-8]\d|9[0-7]|0?[28]|0?9(?=09))(?:\d{2}))$/;

// assume the login primary account can be 1-9 digits, then identify server will auth
export const isAccountNumber = (value: string): boolean => {
    return /^\d{1,9}$/.test(value);
};

export const isValidEmail = (value: string): boolean => {
    return regexEmail.test(value);
};

export const isValidAustralianPhone = (value: string): boolean => {
    return regexPhoneAU.test(value);
};
