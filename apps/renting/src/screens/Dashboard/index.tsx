import { LandPageHeader, ImageListAdBox } from '@monorepo-template/ui/material-components'

// TODO: move land page heading to lib
// TODO: move to middile of page, change emoji, design better style..
export function LandPage() {
  return (
    <div>
      <LandPageHeader projectName="renting" />
      <ImageListAdBox />
    </div>
  );
}

export default LandPage;
