import { useEffect, useState } from 'react';
import { InlineVerifyInputField } from '@monorepo-template/ui/material-components';
import Trail1Card from './Trail1Card';
import Trail2Card from './Trail2Card';
import Monthly from './Monthly';
import Yearly from './Yearly';

import styles from './index.module.css';

// TODO: consider directly use pay link URL to own components, if want more customized style
// TODO: add after pay logic, to record paid user informations!!
export function Pricing() {
  const [coupon, setCoupon] = useState<string>('');
  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://js.stripe.com/v3/buy-button.js';
    script.async = true;
    document.body.appendChild(script);
    return () => {
      // clean up the script when the component in unmounted
      document.body.removeChild(script);
    };
  }, []);

  const couponInputOnChangeHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setCoupon(event.target.value);
  };

  const verifyCouponCode = () => {
    console.log('current coupon', coupon);
  };

  return (
    <div className={styles['pay-btns-list']}>
      <Trail1Card />
      <Trail2Card />
      <Monthly />
      <Yearly />
      <InlineVerifyInputField
        label="Coupon"
        id="couponInputVerify"
        onChange={couponInputOnChangeHandler}
        value={coupon}
        verifyEventHandler={verifyCouponCode}
      />
    </div>
  );
}

export default Pricing;
