import React from 'react';

function Trail1Card() {
  return React.createElement("stripe-buy-button", {
      "buy-button-id": process.env.NX_STRIPE_PAY_BTN_ID_TRAIL1,
      "publishable-key": process.env.NX_STRIPE_PAY_KEY_TRAIL1,
  })
}

export default Trail1Card;