import React from 'react';

function Monthly() {
  return React.createElement("stripe-buy-button", {
      "buy-button-id": process.env.NX_STRIPE_PAY_BTN_ID_MONTHLY,
      "publishable-key": process.env.NX_STRIPE_PAY_KEY_MONTHLY,
  })
}

export default Monthly;