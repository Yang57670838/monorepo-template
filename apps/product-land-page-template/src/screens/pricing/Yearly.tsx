import React from 'react';

function Yearly() {
  return React.createElement('stripe-buy-button', {
    'buy-button-id': process.env.NX_STRIPE_PAY_BTN_ID_YEARLY,
    'publishable-key': process.env.NX_STRIPE_PAY_KEY_YEARLY,
  });
}

export default Yearly;
