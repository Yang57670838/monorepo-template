import React from 'react';

function Trail2Card() {
  return React.createElement("stripe-buy-button", {
      "buy-button-id": process.env.NX_STRIPE_PAY_BTN_ID_TRAIL2,
      "publishable-key": process.env.NX_STRIPE_PAY_KEY_TRAIL2,
  })
}

export default Trail2Card;