// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.css';

import Pricing from '../screens/pricing';

export function App() {
  return (
    <div>
      <Pricing />
    </div>
  );
}

export default App;
